# Przykładowa strona projektu
Głównym plikiem projektu jest index.html 

Kod który dodajemy będzie się znajdował w katalogu *src/*

Zależy mi na tym żeby od początku tworzyć stronę która będzie dobrze wyglądać, gdzie od razu widać że każdy znajdzie miejsce dla siebie :D 


# Biblioteka 
https://github.com/visjs/vis-graph3d


# Inne przykładowe template-y #
https://mdbootstrap.com/freebies/


### Protip: jak szybko commitować
```bash
    git commit -a 
    git push
```
![gif](./docs/vTmPwGx0hy.gif)
